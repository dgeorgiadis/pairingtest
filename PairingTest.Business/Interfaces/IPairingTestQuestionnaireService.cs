﻿using PairingTest.Business.Models;

namespace PairingTest.Business.Interfaces
{
    public interface IPairingTestQuestionnaireService
    {
        QuestionnaireModel GetQuestionnaire();
    }
}