﻿using System;
using System.Net.Http;
using PairingTest.Business.Interfaces;
using PairingTest.Business.Models;
using System.Net.Http.Formatting;
using AutoMapper;
using QuestionServiceWebApi;

namespace PairingTest.Business.Services
{    
    public class WebApiQuestionsService: IPairingTestQuestionnaireService
    {
        private HttpMessageHandler _httpMessageHandler;

        public WebApiQuestionsService(HttpMessageHandler httpMessageHandler)
        {
            _httpMessageHandler = httpMessageHandler;
            Mapper.CreateMap<Questionnaire, QuestionnaireModel>().ReverseMap();
            Mapper.CreateMap<Question, QuestionModel>().ReverseMap();
        }
     
        public QuestionnaireModel GetQuestionnaire()
        {           
            using (var httpClient = new HttpClient(_httpMessageHandler))
            {
                var requestUri = new Uri("http://localhost:50014/api/questions");
                var response = httpClient.GetAsync(requestUri).Result;
                var questionnaire  = response.Content!=null ? response.Content.ReadAsAsync(typeof(Questionnaire)).Result : null;
                var questionnaireModel = Mapper.Map<QuestionnaireModel>(questionnaire);
                return questionnaireModel;
            }
        }
    }
}