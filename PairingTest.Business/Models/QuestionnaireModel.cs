﻿using System.Collections.Generic;

namespace PairingTest.Business.Models
{
    public class QuestionnaireModel
    {        
        public string QuestionnaireTitle { get; set; }
        public IList<QuestionModel> Questions { get; set; }
    }

    public class QuestionModel
    {
        public int Id { get; set; }
        public string Text { get; set; }        
    }
}