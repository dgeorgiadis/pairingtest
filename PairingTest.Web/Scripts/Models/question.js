var PairingTest;
(function (PairingTest) {
    var Question = (function () {
        function Question(data) {
            if (data == null) {
                throw new Error("Cannot initialize with null");
            }
            this.Id = data.Id;
            this.Text = ko.observable(data.Text);
        }
        return Question;
    }());
    PairingTest.Question = Question;
})(PairingTest || (PairingTest = {}));
//# sourceMappingURL=question.js.map