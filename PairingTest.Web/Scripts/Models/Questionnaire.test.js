/// <reference path="questionnaire.ts" />
/// <reference path="../typings/knockout.d.ts" />
/// <reference path="question.ts" />
describe("Questionnaire test", function () {
    it("When initializing a Questionnaire, it's Title should match the Title in the data", function () {
        var questionnairTitle = "Questionnair title";
        var data = { QuestionnaireTitle: questionnairTitle };
        var questionnaire = new PairingTest.Questionnaire(data);
        expect(questionnaire.QuestionnaireTitle()).toBe(questionnairTitle);
    });
    it("When initializing a Questionnaire, it's Questions should match the Questions in the data", function () {
        var data = {
            Questions: [
                { Id: 1, Text: "Question1" },
                { Id: 2, Text: "Question2" }
            ]
        };
        var questionnaire = new PairingTest.Questionnaire(data);
        expect(questionnaire.Questions()[0].Text()).toBe("Question1");
        expect(questionnaire.Questions()[1].Text()).toBe("Question2");
    });
    it("When initializing a Questionnaire with undefined Questions, it should create empty Questions ", function () {
        var questionnairTitle = "Questionnair title";
        var data = { QuestionnaireTitle: questionnairTitle };
        var questionnaire = new PairingTest.Questionnaire(data);
        expect(questionnaire.Questions().length).toBe(0);
    });
    it("When initializing a Questionnaire with null, it should give error 'Cannot initialize with null' ", function () {
        expect(function () { var questionnaire = new PairingTest.Questionnaire(null); })
            .toThrow(new Error("Cannot initialize with null"));
    });
});
//# sourceMappingURL=Questionnaire.test.js.map