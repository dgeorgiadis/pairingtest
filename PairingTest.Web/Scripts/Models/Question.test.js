/// <reference path="question.ts" />
/// <reference path="../typings/knockout.d.ts" />
describe("Question test", function () {
    it("When initializing a Question, it's properties should match the data", function () {
        var questionId = 1;
        var questionText = "Question 1";
        var data = { Id: questionId, Text: questionText };
        var question = new PairingTest.Question(data);
        expect(question.Id).toBe(questionId);
        expect(question.Text()).toBe(questionText);
    });
    it("When initializing a Question with null, it should give error 'Cannot initialize with null' ", function () {
        expect(function () { var question = new PairingTest.Question(null); }).toThrow(new Error("Cannot initialize with null"));
    });
});
//# sourceMappingURL=Question.test.js.map