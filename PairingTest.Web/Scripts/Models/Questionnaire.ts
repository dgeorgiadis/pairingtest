﻿module PairingTest {

    export class Questionnaire {
        QuestionnaireTitle: KnockoutObservable<string>;
        Questions: KnockoutObservableArray<Question>;

        constructor(data: any) {
            if (data == null) {
                throw new Error("Cannot initialize with null");
            }
            this.QuestionnaireTitle = ko.observable(data.QuestionnaireTitle);             
            this.Questions = ko.observableArray(this.getQuestionsArray(data.Questions));
        }

        getQuestionsArray = (data:any) => {
            var questions = new Array<Question>();
            if (data != null && data.length > 0) {
                for (let q of data) {
                    var newQuestion = new Question(q);
                    questions.push(newQuestion);
                }
            }
            return questions;
        }
    }
}