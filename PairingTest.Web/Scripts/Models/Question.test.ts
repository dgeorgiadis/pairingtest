/// <reference path="question.ts" />
/// <reference path="../typings/knockout.d.ts" />

declare var describe, it, expect;

describe("Question test", () => {
    it("When initializing a Question, it's properties should match the data", () => {
        var questionId = 1;    
        var questionText = "Question 1";

        var data = { Id: questionId, Text:  questionText};            

        var question = new PairingTest.Question(data);

        expect(question.Id).toBe(questionId);
        expect(question.Text()).toBe(questionText);
    });



    it("When initializing a Question with null, it should give error 'Cannot initialize with null' ", () => {

        expect( () => { var question = new PairingTest.Question(null); }).toThrow(new Error("Cannot initialize with null"));
    });
})