module PairingTest {
    export class Question {
        Id:number;
        Text: KnockoutObservable<string>;

        constructor(data: any) {
            if (data == null) {
                throw new Error("Cannot initialize with null");
            }
            this.Id = data.Id;
            this.Text = ko.observable(data.Text);
        }

    }
}