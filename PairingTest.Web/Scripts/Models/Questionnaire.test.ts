/// <reference path="questionnaire.ts" />
/// <reference path="../typings/knockout.d.ts" />
/// <reference path="question.ts" />

declare var describe, it, expect, ReSharperReporter;

describe("Questionnaire test",
    () => {

        it("When initializing a Questionnaire, it's Title should match the Title in the data",
            () => {

                var questionnairTitle = "Questionnair title";

                var data = { QuestionnaireTitle: questionnairTitle };

                var questionnaire = new PairingTest.Questionnaire(data);

                expect(questionnaire.QuestionnaireTitle()).toBe(questionnairTitle);
            });

        it("When initializing a Questionnaire, it's Questions should match the Questions in the data",
            () => {
                var data = {
                    Questions: [
                        { Id: 1, Text: "Question1" },
                        { Id: 2, Text: "Question2" }
                    ]
                };

                var questionnaire = new PairingTest.Questionnaire(data);

                expect(questionnaire.Questions()[0].Text()).toBe("Question1");
                expect(questionnaire.Questions()[1].Text()).toBe("Question2");
            });



        it("When initializing a Questionnaire with undefined Questions, it should create empty Questions ",
            () => {
                var questionnairTitle = "Questionnair title";

                var data = { QuestionnaireTitle: questionnairTitle };

                var questionnaire = new PairingTest.Questionnaire(data);

                expect(questionnaire.Questions().length).toBe(0);
            });


        it("When initializing a Questionnaire with null, it should give error 'Cannot initialize with null' ",
            () => {
                expect(() => { var questionnaire = new PairingTest.Questionnaire(null); })
                    .toThrow(new Error("Cannot initialize with null"));
            });


    });