var PairingTest;
(function (PairingTest) {
    var Questionnaire = (function () {
        function Questionnaire(data) {
            this.getQuestionsArray = function (data) {
                var questions = new Array();
                if (data != null && data.length > 0) {
                    for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                        var q = data_1[_i];
                        var newQuestion = new PairingTest.Question(q);
                        questions.push(newQuestion);
                    }
                }
                return questions;
            };
            if (data == null) {
                throw new Error("Cannot initialize with null");
            }
            this.QuestionnaireTitle = ko.observable(data.QuestionnaireTitle);
            this.Questions = ko.observableArray(this.getQuestionsArray(data.Questions));
        }
        return Questionnaire;
    }());
    PairingTest.Questionnaire = Questionnaire;
})(PairingTest || (PairingTest = {}));
//# sourceMappingURL=questionnaire.js.map