﻿using System.Web.Mvc;
using AutoMapper;
using PairingTest.Business.Interfaces;
using PairingTest.Business.Models;
using PairingTest.Web.Models;

namespace PairingTest.Web.Controllers
{
    public class QuestionnaireController : Controller
    {        
        private IPairingTestQuestionnaireService _questionnaireService;
         
        public QuestionnaireController(IPairingTestQuestionnaireService questionnaireService)
        {
            _questionnaireService = questionnaireService;

            Mapper.CreateMap<QuestionnaireModel, QuestionnaireViewModel>().ReverseMap();
            Mapper.CreateMap<QuestionModel, QuestionViewModel>().ReverseMap();
        }
         

        public ViewResult Index()
        {
            var questionnaire = _questionnaireService.GetQuestionnaire();
            var viewModel = Mapper.Map<QuestionnaireViewModel>(questionnaire);
            return View(viewModel);
        }
    }
}
