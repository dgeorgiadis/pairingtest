﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Owin.Hosting;
using NUnit.Framework;
using QuestionServiceWebApi;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi.SelfHost
{
    [TestFixture]
    public class When_Reading_Questions
    {
        private static IDisposable _webApp;

        [SetUp]
        public void Setup()
        {
            _webApp = WebApp.Start<Startup>("http://*:9443/");

        }


        [Test]
        public void GetQuestion_should_return_status_OK()
        {
            using (var httpClient = new HttpClient())
            {                               
                var requestUri = new Uri("http://localhost:9443/api/questions");
                var status = httpClient.GetAsync(requestUri).Result.StatusCode;
               

                Assert.That(status, Is.EqualTo(HttpStatusCode.OK));
            }
        }

        [Test]
        public void GetQuestion_should_return_expected_questionnaire_title()
        {
            using (var httpClient = new HttpClient())
            {
                var expectedQuestionnaireTitle = "Geography Questions";              
                var requestUri = new Uri("http://localhost:9443/api/questions");
                var questionnaire = httpClient.GetAsync(requestUri).Result.Content.ReadAsAsync(typeof(Questionnaire)).Result;

                Assert.That(((Questionnaire)questionnaire).QuestionnaireTitle, Is.EqualTo(expectedQuestionnaireTitle));
            }
        }


        [Test]
        public void GetQuestion_should_return_expected_questions()
        {
            using (var httpClient = new HttpClient())
            {
                var expectedQuestionnaireTitle = "Geography Questions";              
                var requestUri = new Uri("http://localhost:9443/api/questions");
                var questionnaire = httpClient.GetAsync(requestUri).Result.Content.ReadAsAsync(typeof(Questionnaire)).Result as Questionnaire;
                Assert.That(questionnaire, Is.Not.EqualTo(null));
                Assert.That(questionnaire.Questions[0].Text, Is.EqualTo("What is the capital of Cuba?"));
                Assert.That(questionnaire.Questions[1].Text, Is.EqualTo("What is the capital of France?"));
                Assert.That(questionnaire.Questions[2].Text, Is.EqualTo("What is the capital of Poland?"));
                Assert.That(questionnaire.Questions[3].Text, Is.EqualTo("What is the capital of Germany?"));
            }
        }

        [TearDown]
        public void Cleanup()
        {
            _webApp.Dispose();
        }
    }
}