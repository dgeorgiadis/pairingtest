﻿using System.Collections.Generic;
using QuestionServiceWebApi;
using QuestionServiceWebApi.Interfaces;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi.Repositories
{
    public class FakeQuestionRepository : IQuestionRepository
    {
        public FakeQuestionRepository()
        {
            ExpectedQuestions =  new Questionnaire
            {
                QuestionnaireTitle = "Questionnaire 1",
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 1,
                        Text = "Question1?"
                    },
                    new Question
                    {
                        Id = 2,
                        Text = "Question2?"
                    },
                    new Question
                    {
                        Id = 3,
                        Text = "Question3?"
                    },
                    new Question
                    {
                        Id = 4,
                        Text = "Question4?"
                    }
                }
            };
        }
        
        public Questionnaire ExpectedQuestions { get; set; }
        public Questionnaire GetQuestionnaire()
        {
            return ExpectedQuestions;
        }
    }
}