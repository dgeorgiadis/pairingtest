﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Owin.Hosting;
using NUnit.Framework;
using PairingTest.Unit.Tests.QuestionServiceWebApi.Repositories;
using QuestionServiceWebApi;
using QuestionServiceWebApi.Controllers;
//using System.Net.Http.Formatting.Extension;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    [TestFixture]
    public class QuestionsControllerTests
    {
       

        [Test]
        public void Should_get_expected_questionnaire()
        {
            //Arrange            
            var fakeQuestionRepository = new FakeQuestionRepository();
            var questionsController = new QuestionsController(fakeQuestionRepository);
            var expectedTitle = fakeQuestionRepository.ExpectedQuestions.QuestionnaireTitle;

            //Act
            var questionnaire = questionsController.Get();

            //Assert
            Assert.That(questionnaire.QuestionnaireTitle, Is.EqualTo(expectedTitle));
             
                
         } 

        [Test]
        public void Should_get_expected_questions()
        {
            //Arrange            
            var fakeQuestionRepository = new FakeQuestionRepository();
            var questionsController = new QuestionsController(fakeQuestionRepository);
            

            //Act
            var questionnaire = questionsController.Get();

            //Assert
            Assert.That(questionnaire.Questions[0].Text, Is.EqualTo("Question1?"));
            Assert.That(questionnaire.Questions[1].Text, Is.EqualTo("Question2?"));
            Assert.That(questionnaire.Questions[2].Text, Is.EqualTo("Question3?"));
            Assert.That(questionnaire.Questions[3].Text, Is.EqualTo("Question4?"));

        }
      

     
    }
}