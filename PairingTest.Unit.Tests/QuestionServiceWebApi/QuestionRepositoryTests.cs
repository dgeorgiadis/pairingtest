﻿using NUnit.Framework;
using PairingTest.Unit.Tests.QuestionServiceWebApi.Repositories;
using QuestionServiceWebApi;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    [TestFixture]
    public class QuestionRepositoryTests
    {
        [Test]
        public void Should_Get_Expected_Questionnaire()
        {
            // Arrange
            var questionRepository = new QuestionRepository();


            // Act
            var questionnaire = questionRepository.GetQuestionnaire();

            // Assert           
            Assert.That(questionnaire.QuestionnaireTitle, Is.EqualTo("Geography Questions"));
            Assert.That(questionnaire.Questions[0].Text, Is.EqualTo("What is the capital of Cuba?"));
            Assert.That(questionnaire.Questions[1].Text, Is.EqualTo("What is the capital of France?"));
            Assert.That(questionnaire.Questions[2].Text, Is.EqualTo("What is the capital of Poland?"));
            Assert.That(questionnaire.Questions[3].Text, Is.EqualTo("What is the capital of Germany?"));
        }
    }
}