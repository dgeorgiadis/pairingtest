﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PairingTest.Unit.Tests
{
    public class GWT
    {
        [SetUp]
        public void Setup()
        {
            Given();
            When();
        }

        [TearDown]
        protected void Teardown()
        {
            Cleanup();
        }

        protected virtual void Given()
        {

        }
        protected virtual void When()
        {

        }
        protected virtual void Cleanup()
        {

        }

    }
}
