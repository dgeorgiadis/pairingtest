﻿using NUnit.Framework;
using PairingTest.Business.Services;

namespace PairingTest.Unit.Tests.Business
{
    public class WebApiQuestionsServiceTests
    {      
        [Test]
        public void GetQuestionnaire_should_invoke_GET_method_on_api_service()
        {
            // Arrange
            var spy = new SpyHttpMessageHandler();
            var service = new WebApiQuestionsService(spy);

            // Act
            service.GetQuestionnaire();

            // Assert
            Assert.That(spy.MethodInvoked.ToUpper(), Is.EqualTo("GET"));
        }
    }
}