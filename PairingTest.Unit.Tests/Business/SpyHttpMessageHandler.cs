﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PairingTest.Unit.Tests.Business
{
    public class SpyHttpMessageHandler : HttpMessageHandler
    {
        public string MethodInvoked = "";
        public HttpRequestMessage RequestMessage { get; private set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            this.MethodInvoked = request.Method.ToString();
            RequestMessage = request;
            return Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK));
        }
        


    }
}