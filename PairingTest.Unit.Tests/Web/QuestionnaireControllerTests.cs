﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Moq;
using NUnit.Framework;
using PairingTest.Business.Interfaces;
using PairingTest.Business.Models;
using PairingTest.Unit.Tests.QuestionServiceWebApi.Repositories;
using PairingTest.Web.Controllers;
using PairingTest.Web.Models;

namespace PairingTest.Unit.Tests.Web
{
    [TestFixture]
    public class QuestionnaireControllerTests
    {
        private QuestionnaireModel _questionnaire;

        [SetUp]
        public void Setup()
        {
            _questionnaire = new QuestionnaireModel()
            {
                QuestionnaireTitle = "My expected questionnaire",
                Questions = new List<QuestionModel>()
                {
                    new QuestionModel()
                    {
                        Id = 1,
                        Text = "Question 1"
                    },
                    new QuestionModel()
                    {
                        Id = 2,
                        Text = "Question 2"
                    }
                }
            };
        }


        [Test]
        public void Should_return_expected_questionnaire_model_title()
        {
            //Arrange            
            var questionnaireServiceMock = new Mock<IPairingTestQuestionnaireService>();
            questionnaireServiceMock.Setup(x => x.GetQuestionnaire()).Returns(_questionnaire);
            var questionnaireController = new QuestionnaireController(questionnaireServiceMock.Object);

            //Act
            var result = (QuestionnaireViewModel)questionnaireController.Index().ViewData.Model;
            
            //Assert
            Assert.That(result.QuestionnaireTitle, Is.EqualTo(_questionnaire.QuestionnaireTitle));
        }


        [Test]
        public void Should_return_expected_question_models()
        {
            //Arrange            
            var questionnaireServiceMock = new Mock<IPairingTestQuestionnaireService>();
            questionnaireServiceMock.Setup(x => x.GetQuestionnaire()).Returns(_questionnaire);
            var questionnaireController = new QuestionnaireController(questionnaireServiceMock.Object);

            //Act
            var result = (QuestionnaireViewModel)questionnaireController.Index().ViewData.Model;
            
            //Assert
            Assert.That(result.Questions[0].Text, Is.EqualTo(_questionnaire.Questions[0].Text));
            Assert.That(result.Questions[1].Text, Is.EqualTo(_questionnaire.Questions[1].Text));
        }

      

        [TearDown]
        public void Cleanup()
        {
         
        }
    }
}