﻿using System;
using System.Net.Http;
using Microsoft.Owin.Hosting;
using PairingTest.Business.Services;
using PairingTest.Unit.Tests.Business;
using PairingTest.Web.Controllers;
using QuestionServiceWebApi;

namespace PairingTest.Unit.Tests.Web.AcceptanceTests
{
    public class Questionnaire_with_three_questions:GWT
    {
        protected static IDisposable _webApp;
        protected QuestionnaireController _questionnaireController;


        protected  override void Given()
        {
            base.Given();
            _webApp = WebApp.Start<Startup>("http://*:50014/");
            _questionnaireController = new QuestionnaireController( new WebApiQuestionsService(new HttpClientHandler()));
        }

        protected override void Cleanup()
        {
            _webApp.Dispose();
        }
        
    }
}