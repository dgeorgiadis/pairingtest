﻿using NUnit.Framework;
using PairingTest.Web.Models;

namespace PairingTest.Unit.Tests.Web.AcceptanceTests
{
    public class When_accessing_index_on_questionnaire : Questionnaire_with_three_questions
    {
        private QuestionnaireViewModel _resultQuestionnaireViewModel;

        protected override void When()
        {
            base.When();
             _resultQuestionnaireViewModel = (QuestionnaireViewModel)_questionnaireController.Index().ViewData.Model;
        }

        [Test]
        public void Should_return_expected_questionnaire_title()
        {
           Assert.That(_resultQuestionnaireViewModel.QuestionnaireTitle, Is.EqualTo("Geography Questions"));
        }


        [Test]
        public void Should_return_four_questions()
        {
           Assert.That(_resultQuestionnaireViewModel.Questions.Count, Is.EqualTo(4));
        }

        [Test]
        public void Should_return_expected_questions()
        {
           Assert.That(_resultQuestionnaireViewModel.Questions[0].Text, Is.EqualTo("What is the capital of Cuba?"));
           Assert.That(_resultQuestionnaireViewModel.Questions[1].Text, Is.EqualTo("What is the capital of France?"));
           Assert.That(_resultQuestionnaireViewModel.Questions[2].Text, Is.EqualTo("What is the capital of Poland?"));
           Assert.That(_resultQuestionnaireViewModel.Questions[3].Text, Is.EqualTo("What is the capital of Germany?"));
        }


    }
}