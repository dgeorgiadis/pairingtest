using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Owin;
using Owin;
using QuestionServiceWebApi;

//[assembly: OwinStartup(typeof(Startup))]
namespace QuestionServiceWebApi
{
    // used for tests

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();
            MvcApplication.RegisterContainer(configuration);
            WebApiConfig.Register(configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      
            app.UseWebApi(configuration);
        }
    }
}