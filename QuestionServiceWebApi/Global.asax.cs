﻿using System.Runtime.Remoting.Channels;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Owin;
using QuestionServiceWebApi.Interfaces;
using StructureMap;
using WebApi.StructureMap;


namespace QuestionServiceWebApi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        

        protected void Application_Start()
        {
            var config = GlobalConfiguration.Configuration;
            RegisterContainer(config);
            WebApiConfig.Register(config);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);            
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

        }

        public static void RegisterContainer(HttpConfiguration config)
        {
            var container = new Container(
                c=> c.For<IQuestionRepository>().Use<QuestionRepository>()
                );
            config.DependencyResolver = new WebApi.StructureMap.DependencyResolver(container);
        }

    }

   
}