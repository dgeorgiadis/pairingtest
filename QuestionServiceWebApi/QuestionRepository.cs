using System.Collections.Generic;
using System.Net;
using QuestionServiceWebApi.Interfaces;

namespace QuestionServiceWebApi
{
    public class QuestionRepository : IQuestionRepository
    {


        public Questionnaire GetQuestionnaire()
        {
          //  var server = new HttpListener();
          //  server.Start();


            return new Questionnaire
            {
                QuestionnaireTitle = "Geography Questions",
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 1,
                        Text = "What is the capital of Cuba?"
                    },
                    new Question
                    {
                        Id = 2,
                        Text = "What is the capital of France?"
                    },
                    new Question
                    {
                        Id = 3,
                        Text = "What is the capital of Poland?"
                    },
                    new Question
                    {
                        Id = 4,
                        Text = "What is the capital of Germany?"
                    }
                }
            };
        }
    }
}